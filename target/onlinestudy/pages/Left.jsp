<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>无标题页</TITLE>
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<STYLE type=text/css>
{
FONT-SIZE
:
12
px
}
#menuTree A {
	COLOR: #566984;
	TEXT-DECORATION: none
}
</STYLE>
<SCRIPT src="Left.files/TreeNode.js" type=text/javascript></SCRIPT>
<SCRIPT src="Left.files/Tree.js" type=text/javascript></SCRIPT>
<META content="MSHTML 6.00.2900.5848" name=GENERATOR>
</HEAD>
<BODY
	style="BACKGROUND-POSITION-Y: -120px; BACKGROUND-IMAGE: url(../images/bg.gif); BACKGROUND-REPEAT: repeat-x">
	<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%">
		<TBODY>
			<TR>
				<TD width=10 height=29><IMG src="Left.files/bg_left_tl.gif"></TD>
				<TD
					style="FONT-SIZE: 18px; BACKGROUND-IMAGE: url(../images/bg_left_tc.gif); COLOR: white; FONT-FAMILY: system">Main
					Menu</TD>
				<TD width=10><IMG src="Left.files/bg_left_tr.gif"></TD>
			</TR>
			<TR>
				<TD style="BACKGROUND-IMAGE: url(../images/bg_left_ls.gif)"></TD>
				<TD id=menuTree
					style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px; HEIGHT: 100%; BACKGROUND-COLOR: white"
					vAlign=top></TD>
				<TD style="BACKGROUND-IMAGE: url(../images/bg_left_rs.gif)"></TD>
			</TR>
			<TR>
				<TD width=10><IMG src="Left.files/bg_left_bl.gif"></TD>
				<TD style="BACKGROUND-IMAGE: url(../images/bg_left_bc.gif)"></TD>
				<TD width=10><IMG src="Left.files/bg_left_br.gif"></TD>
			</TR>
		</TBODY>
	</TABLE>
	<SCRIPT type=text/javascript>
var tree = null;
var root = new TreeNode('系统管理');
var fun1 = new TreeNode('试题数据管理');
var fun2 = new TreeNode('添加考试类型', 'addExamType.jsp', 'tree_node.gif', null, 'tree_node.gif', null);
fun1.add(fun2);
var fun3 = new TreeNode('浏览全部试题', 'lookExam.jsp', 'tree_node.gif', null, 'tree_node.gif', null);
fun1.add(fun3);
var fun4 = new TreeNode('添加试题', 'addTexam.jsp', 'tree_node.gif', null, 'tree_node.gif', null);
fun1.add(fun4);
root.add(fun1);
var fun5 = new TreeNode('考生数据管理');
var fun6 = new TreeNode('浏览全部考生', 'lookStudent.jsp', 'tree_node.gif', null, 'tree_node.gif', null);
fun5.add(fun6);
var fun7 = new TreeNode('添加考生数据', 'addStudent.jsp', 'tree_node.gif', null, 'tree_node.gif', null);
fun5.add(fun7);
/*var fun8 = new TreeNode('', 'MyNote.aspx', 'tree_node.gif', null, 'tree_node.gif', null);
*//*fun5.add(fun8);*/
root.add(fun5);
tree = new Tree(root);tree.show('menuTree')
</SCRIPT>
</BODY>
</HTML>
