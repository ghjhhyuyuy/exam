package org.wingstudio.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.sql.SQLException;

/**
 * Created by wzw on 2018/10/8
 */

public interface PaperService {
    /**
     * 查找全部试卷
     * @return
     */
    JSONArray AllPaper() throws SQLException;

    /**
     * 根据试卷id查找对应试题
     * @param id
     * @return
     */
    JSONArray AllProblem(int id) throws SQLException;
    /**
     * 查找全部题库
     */
    JSONArray AllBank() throws SQLException;
    /**
     * 查找对应题库的全部题
     */
    JSONArray AllBankProblem(int bid) throws SQLException;
    /**
     * 根据关键字查找题
     */
    JSONObject selectByWords(String words);
    /**
     * 查找某一试卷的得分情况
     */
    JSONArray selectScore(int id) throws SQLException;
    /**
     * 获取某一试卷的考生名单
     */
    JSONArray selectStudents(int id) throws SQLException;
    /**
     * 按班级分考生
     */
    JSONArray divideClass(JSONArray jsonArray);
    /**
     * 查找指定考生的全部答卷
     */
    JSONArray manPaper(int uid) throws SQLException;
    /**
     * 查找指定考生的指定考卷
     */
    JSONArray paperDetails(int uid,int id) throws SQLException;
    /**
     * 插入考生回答
     */
    String insertAnswer(int uid,int pid,String answer,int id);
    /**
     * 添加问题
     */
    String addProblem(String topic,int id,int type,String answer,int fen);
    /**
     * 添加试卷
     */
    String addPaper(String name);
    /**
     * 获取试题名
     */
    String getPaperName(int id);
}
