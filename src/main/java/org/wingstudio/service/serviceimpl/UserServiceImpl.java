package org.wingstudio.service.serviceimpl;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wingstudio.dao.LimitMapper;
import org.wingstudio.dao.UserMapper;
import org.wingstudio.entity.Limit;
import org.wingstudio.entity.User;
import org.wingstudio.service.UserService;

/**
 * Created by wzw on 2018/10/8
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    LimitMapper limitMapper;
    @Override
    public JSONObject FindInformation(int uid) {
        JSONObject jsonObject = new JSONObject();
        User user = userMapper.selectByPrimaryKey(uid);
        jsonObject.put("account",user.getAccount());
        jsonObject.put("class",user.getClass());
        jsonObject.put("shenfeng",user.getLid());
        return jsonObject;
    }

    @Override
    public String Login(String account,String password) {
        User user = userMapper.selectByAccount(account);
        if(user.getPassword().equals(password)){
            return "";
        }
        else{
            return null;
        }
    }

    @Override
    public String getName(int uid) {
        String account = userMapper.selectAccountByUid(uid);
        return account;
    }

    @Override
    public String getIdentity(int uid) {
        int lid = userMapper.selectLidByUid(uid);
        Limit limit = limitMapper.selectByPrimaryKey(lid);
        return limit.getLimit();
    }
}
