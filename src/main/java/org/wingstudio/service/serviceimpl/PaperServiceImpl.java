package org.wingstudio.service.serviceimpl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wingstudio.dao.*;
import org.wingstudio.entity.*;
import org.wingstudio.service.PaperService;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by wzw on 2018/10/8
 */
@Service
public class PaperServiceImpl implements PaperService {
    @Autowired
    PaperMapper paperMapper;
    ProblemMapper problemMapper;
    BankMapper bankMapper;
    ResultMapper resultMapper;
    UserMapper userMapper;
    AnswerMapper answerMapper;
    ShitiMapper shitiMapper;
    @Override
    public JSONArray AllPaper() throws SQLException {
        ResultSet resultSet = paperMapper.selectAll();
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("paper",resultSet.getString("id"));
            jsonObject.put("paper",resultSet.getString("name"));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public JSONArray AllProblem(int id) throws SQLException {
        Paper paper = paperMapper.selectByPrimaryKey(id);
        JSONArray jsonArray = new JSONArray();
        ResultSet resultSet = paperMapper.selectProblem(paper.getId());
        //通过pid获取问题信息
        while (resultSet.next()){
            Problem problem = problemMapper.selectByPrimaryKey(resultSet.getInt("pid"));
            //通过题号获取题目、类型、编号、答案
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("topic",problem.getTopic());
            jsonObject.put("type",problem.getType());
            jsonObject.put("pid",problem.getPid());
            jsonObject.put("answer",problem.getAnswer());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public JSONArray AllBank() throws SQLException {
        ResultSet resultSet = bankMapper.selectAll();
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("bid",resultSet.getInt("bid"));
            jsonObject.put("pid",resultSet.getInt("pid"));
            jsonObject.put("bnamep",resultSet.getInt("bname"));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public JSONArray AllBankProblem(int bid) throws SQLException {
        ResultSet resultSet = bankMapper.selectPid(bid);
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()){
            Problem problem = problemMapper.selectByPrimaryKey(resultSet.getInt("pid"));
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("pid",problem.getPid());
            jsonObject.put("topic",problem.getTopic());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public JSONObject selectByWords(String words) {
//        Problem problem = problemMapper.selectByWords(words);
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("problem",problem);
//        return jsonObject;
        return null;
    }

    @Override
    public JSONArray selectScore(int id) throws SQLException {
        ResultSet resultSet = resultMapper.selectById(id);
        JSONArray jsonArray = new JSONArray();
        while(resultSet.next()){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("score",resultSet.getInt("score"));
            jsonObject.put("uid",resultSet.getInt("uid"));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public JSONArray selectStudents(int id) throws SQLException {
        ResultSet resultSet = resultMapper.selectUid(id);
        JSONArray jsonArray = new JSONArray();
        while(resultSet.next()){
            User user = userMapper.selectByPrimaryKey(resultSet.getInt("uid"));
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("account",user.getAccount());
            jsonObject.put("uid",resultSet.getInt("uid"));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public JSONArray divideClass(JSONArray jsonArray) {
        //json解析，确定格式
        for(int i =0 ;i<jsonArray.size();i++){
            JSONObject jsonObject = new JSONObject();
        }
        return null;
    }

    @Override
    public JSONArray manPaper(int uid) throws SQLException {
        ResultSet resultSet = answerMapper.selectPaper(uid);
        JSONArray jsonArray = new JSONArray();
        while(resultSet.next()){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",paperMapper.selectNameByPrimaryKey(resultSet.getInt("id")));
            jsonObject.put("id",resultSet.getInt("id"));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public JSONArray paperDetails(int uid, int id) throws SQLException {
        ResultSet resultSet = answerMapper.selectDetails(uid,id);
        JSONArray jsonArray = new JSONArray();
        while(resultSet.next()){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("pid",resultSet.getInt("pid"));
            jsonObject.put("answer",resultSet.getString("answer"));
            jsonObject.put("topic",problemMapper.selectTopicByPrimaryKey(resultSet.getInt("pid")));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public String insertAnswer(int uid,int pid,String answer,int id) {
        Answer answer1 = new Answer();
        answer1.setAnswer(answer);
        answer1.setId(id);
        answer1.setPid(pid);
        answer1.setUid(uid);
        int result = answerMapper.insert(answer1);
        if (result==0){
            return "成功";
        }
        else{
            return "错误";
        }
    }

    @Override
    public String addProblem(String topic, int id, int type, String answer, int fen) {
        Problem problem = new Problem();
        problem.setAnswer(answer);
        problem.setTopic(topic);
        problem.setType(type);
        problemMapper.insert(problem);
        int pid = problemMapper.selectPidByTopic(topic);
        Shiti shiti = new Shiti();
        shiti.setId(id);
        shiti.setPid(pid);
        int result = shitiMapper.insert(shiti);
        if (result==0){
            return "成功";
        }
        else{
            return "错误";
        }
    }

    @Override
    public String addPaper(String name) {
        Paper paper = new Paper();
        paper.setName(name);
        int result = paperMapper.insert(paper);
        if (result==0){
            return "成功";
        }
        else{
            return "错误";
        }
    }

    @Override
    public String getPaperName(int id) {
        String name = paperMapper.selectNameByPrimaryKey(id);
        return name;
    }
}
