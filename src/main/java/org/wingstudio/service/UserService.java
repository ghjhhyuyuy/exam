package org.wingstudio.service;

import com.alibaba.fastjson.JSONObject;


/**
 * Created by wzw on 2018/10/8
 */
public interface UserService {
    /**
     * 获取个人信息
     * @param uid
     * @return
     */
    JSONObject FindInformation(int uid);

    /**
     * 登录验证
     * @param account
     * @param password
     * @return
     */
    String Login(String account,String password);
    /**
     * 获取用户名
     */
    String getName(int uid);
    /**
     * 获取身份
     */
    String getIdentity(int uid);

}
