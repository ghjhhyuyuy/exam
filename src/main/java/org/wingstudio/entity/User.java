package org.wingstudio.entity;

public class User {
    private Integer uid;

    private String account;

    private String password;

    private Integer lid;

    private Integer stuclass;

    public User(Integer uid, String account, String password, Integer lid, Integer stuclass) {
        this.uid = uid;
        this.account = account;
        this.password = password;
        this.lid = lid;
        this.stuclass = stuclass;
    }

    public User() {
        super();
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public Integer getLid() {
        return lid;
    }

    public void setLid(Integer lid) {
        this.lid = lid;
    }

    public Integer getStuclass() {
        return stuclass;
    }

    public void setStuclass(Integer stuclass) {
        this.stuclass = stuclass;
    }
}