package org.wingstudio.entity;

public class Bank {
    private Integer bid;

    private Integer pid;

    private String bname;

    public Bank(Integer bid, Integer pid, String bname) {
        this.bid = bid;
        this.pid = pid;
        this.bname = bname;
    }

    public Bank() {
        super();
    }

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname == null ? null : bname.trim();
    }
}