package org.wingstudio.entity;

public class Problem {
    private Integer pid;

    private Integer type;

    private String topic;

    private String answer;

    private Integer fen;

    public Problem(Integer pid, Integer type, String topic, String answer, Integer fen) {
        this.pid = pid;
        this.type = type;
        this.topic = topic;
        this.answer = answer;
        this.fen = fen;
    }

    public Problem() {
        super();
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic == null ? null : topic.trim();
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }

    public Integer getFen() {
        return fen;
    }

    public void setFen(Integer fen) {
        this.fen = fen;
    }
}