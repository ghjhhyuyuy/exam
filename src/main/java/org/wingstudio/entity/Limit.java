package org.wingstudio.entity;

public class Limit {
    private Integer lid;

    private String limit;

    public Limit(Integer lid, String limit) {
        this.lid = lid;
        this.limit = limit;
    }

    public Limit() {
        super();
    }

    public Integer getLid() {
        return lid;
    }

    public void setLid(Integer lid) {
        this.lid = lid;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit == null ? null : limit.trim();
    }
}