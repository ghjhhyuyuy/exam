package org.wingstudio.entity;

public class Answer {
    private Integer aid;

    private Integer id;

    private Integer pid;

    private Integer uid;

    private String answer;

    public Answer(Integer aid, Integer id, Integer pid, Integer uid, String answer) {
        this.aid = aid;
        this.id = id;
        this.pid = pid;
        this.uid = uid;
        this.answer = answer;
    }

    public Answer() {
        super();
    }

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }
}