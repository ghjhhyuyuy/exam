package org.wingstudio.entity;

public class Result {
    private Integer rid;

    private Integer id;

    private Integer score;

    public Result(Integer rid, Integer id, Integer score) {
        this.rid = rid;
        this.id = id;
        this.score = score;
    }

    public Result() {
        super();
    }

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}