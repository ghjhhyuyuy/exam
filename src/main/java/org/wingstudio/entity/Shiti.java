package org.wingstudio.entity;

public class Shiti {
    private Integer sid;

    private Integer id;

    private Integer pid;

    public Shiti(Integer sid, Integer id, Integer pid) {
        this.sid = sid;
        this.id = id;
        this.pid = pid;
    }

    public Shiti() {
        super();
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }
}