package org.wingstudio.entity.requestModal;

/**
 * @Author beifengtz
 * @Date Created in 18:53 2018/6/26
 * @Description:
 */
public class ResultVO {
    private Integer code;

    private String msg;

    private Object data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
