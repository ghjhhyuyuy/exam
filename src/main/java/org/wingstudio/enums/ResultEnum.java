package org.wingstudio.enums;

/**
 * @Author beifengtz
 * @Date Created in 19:25 2018/6/26
 * @Description:
 */
public enum ResultEnum {

    UNKNOWE_ERROR(-1,"未知错误"),
    SUCCESS(0,"成功"),
    PARAM_ERROR(1,"参数错误"),
    LOGIN_ERROR(2,"登录错误"),
    INVALID_TOKEN(3,"无效的token"),
    USER_NOT_FOUND(4,"用户不存在"),
    PASSWORD_ERROR(5,"密码错误"),
    RESOURCE_NOT_FOUND(6,"资源不存在"),
    UNAUTHORIZED(7,"没有权限"),
    USER_EXISTS(8,"用户已存在");
    private int code;

    private String msg;

    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
