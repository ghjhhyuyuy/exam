package org.wingstudio.dao;

import org.wingstudio.entity.Limit;

public interface LimitMapper {
    int deleteByPrimaryKey(Integer lid);

    int insert(Limit record);

    int insertSelective(Limit record);

    Limit selectByPrimaryKey(Integer lid);

    int updateByPrimaryKeySelective(Limit record);

    int updateByPrimaryKey(Limit record);
}