package org.wingstudio.dao;

import org.wingstudio.entity.Bank;

import java.sql.ResultSet;

public interface BankMapper {
    int deleteByPrimaryKey(Integer bid);

    int insert(Bank record);

    int insertSelective(Bank record);

    Bank selectByPrimaryKey(Integer bid);

    int updateByPrimaryKeySelective(Bank record);

    int updateByPrimaryKey(Bank record);

    ResultSet selectAll();

    /**
     * 查找一个题库下有哪些试题编号
     */
    ResultSet selectPid(int id);
}