package org.wingstudio.dao;

import org.wingstudio.entity.Result;

import java.sql.ResultSet;

public interface ResultMapper {
    int deleteByPrimaryKey(Integer rid);

    int insert(Result record);

    int insertSelective(Result record);

    Result selectByPrimaryKey(Integer rid);

    int updateByPrimaryKeySelective(Result record);

    int updateByPrimaryKey(Result record);

    ResultSet selectById(int id);

    ResultSet selectUid(int id);
}