package org.wingstudio.dao;

import org.wingstudio.entity.Shiti;

public interface ShitiMapper {
    int deleteByPrimaryKey(Integer sid);

    int insert(Shiti record);

    int insertSelective(Shiti record);

    Shiti selectByPrimaryKey(Integer sid);

    int updateByPrimaryKeySelective(Shiti record);

    int updateByPrimaryKey(Shiti record);
}