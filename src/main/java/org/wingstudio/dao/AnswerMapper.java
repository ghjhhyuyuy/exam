package org.wingstudio.dao;

import org.wingstudio.entity.Answer;

import java.sql.ResultSet;

public interface AnswerMapper {
    int deleteByPrimaryKey(Integer aid);

    int insert(Answer record);

    int insertSelective(Answer record);

    Answer selectByPrimaryKey(Integer aid);

    int updateByPrimaryKeySelective(Answer record);

    int updateByPrimaryKey(Answer record);

    ResultSet selectPaper(int uid);

    ResultSet selectDetails(int uid, int id);
}