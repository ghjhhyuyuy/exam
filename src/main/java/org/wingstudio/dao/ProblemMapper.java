package org.wingstudio.dao;

import org.wingstudio.entity.Problem;

public interface ProblemMapper {
    int deleteByPrimaryKey(Integer pid);

    int insert(Problem record);

    int insertSelective(Problem record);

    Problem selectByPrimaryKey(Integer pid);

    int updateByPrimaryKeySelective(Problem record);

    int updateByPrimaryKey(Problem record);

    /**
     * 根据关键字查找题
     */
    //Problem selectByWords(String words);

    /**
     * 根据pid找题干
     * @param pid
     * @return
     */
    String selectTopicByPrimaryKey(int pid);

    int selectPidByTopic(String topic);
}