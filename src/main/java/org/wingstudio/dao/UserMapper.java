package org.wingstudio.dao;

import org.wingstudio.entity.User;

public interface UserMapper {
    int deleteByPrimaryKey(Integer uid);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer uid);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User selectByAccount(String account);

    String selectAccountByUid(int uid);

    int selectLidByUid(int uid);
}