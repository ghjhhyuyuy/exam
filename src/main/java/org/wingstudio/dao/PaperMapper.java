package org.wingstudio.dao;

import org.wingstudio.entity.Paper;

import java.sql.ResultSet;

public interface PaperMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Paper record);

    int insertSelective(Paper record);

    Paper selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Paper record);

    int updateByPrimaryKey(Paper record);

    ResultSet selectAll();

    ResultSet selectProblem(int id);

    String selectNameByPrimaryKey(int id);
}