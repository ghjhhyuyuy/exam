package org.wingstudio.util;


import org.wingstudio.entity.requestModal.ResultVO;
import org.wingstudio.enums.ResultEnum;

/**
 * @Author beifengtz
 * @Date Created in 19:33 2018/6/26
 * @Description:
 */
public class ResultUtil {

    static public ResultVO responseResult(ResultEnum resultEnum){
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(resultEnum.getCode());
        resultVO.setMsg(resultEnum.getMsg());
        resultVO.setData(null);
        return resultVO;
    }

    static public ResultVO responseResult(ResultEnum resultEnum,String msg){
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(resultEnum.getCode());
        resultVO.setMsg(msg);
        resultVO.setData(null);
        return resultVO;
    }

    static public ResultVO responseResult(ResultEnum resultEnum,Object data){
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(resultEnum.getCode());
        resultVO.setMsg(resultEnum.getMsg());
        resultVO.setData(data);
        return resultVO;
    }

    static public ResultVO responseResult(ResultEnum resultEnum,String msg,Object data){
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(resultEnum.getCode());
        resultVO.setMsg(msg);
        resultVO.setData(data);
        return resultVO;
    }
}
