package org.wingstudio.util;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by wzw on 2018/8/25
 */
public class DBUtil {
    private static ComboPooledDataSource cpds=null;

    static{
        cpds=new ComboPooledDataSource("mysql");
    }

    public static Connection getConnection(){
        Connection connection=null;
        try {
            connection = cpds.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(Connection conn, PreparedStatement pstmt, ResultSet rs){
        try {
            if(rs!=null){
                rs.close();
            }
            if(pstmt!=null){
                pstmt.close();
            }
            if(rs!=null){
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
