package org.wingstudio.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.wingstudio.service.PaperService;
import org.wingstudio.service.UserService;

import java.sql.SQLException;

/**
 * Created by wzw on 2018/10/8
 */
@Controller
public class StudentController {
    @Autowired
    PaperService paperService;
    UserService userService;
    /**
     * 添加考生/add_student
     */
    /**
     * 删除考生/delete_student
     */
    /**
     * 显示主页面
     */
    @RequestMapping("/to_index")
    public String toStudents(){
        return "Left";
    }
    /**
     * 显示全部试卷
     */
    @RequestMapping("/show_paper")
    public JSONArray showPaper() throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray = paperService.AllPaper();
//        modelAndView.addObject("paper",jsonArray);
//        modelAndView.setViewName("examIndex");
        return jsonArray;//id和name
    }
    /**
     * 显示对应试卷下的题
     */
    @RequestMapping("/show_problem")
    public JSONArray showProblem(int id) throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray = paperService.AllProblem(id);
//        modelAndView.addObject("problem",jsonArray);
//        modelAndView.setViewName("lookExam");
        return jsonArray;
    }
    /**
     * /show_paper
     */
    /**
     * 记录答题情况,返回分数？？？
     */
    @RequestMapping("/get_information")
    public String getInformation(int uid,int pid,String answer,int id){
        ModelAndView modelAndView = new ModelAndView();
        String result = paperService.insertAnswer(uid,pid,answer,id);
        return result;
    }
    /**
     * 显示个人信息
     */
    @RequestMapping("/show_information")
    public JSONObject showInformation(int uid){
        ModelAndView modelAndView = new ModelAndView();
        JSONObject jsonObject = userService.FindInformation(uid);
//        modelAndView.addObject("information",jsonObject);
////        //跳转？？？
        return jsonObject;
    }
}
