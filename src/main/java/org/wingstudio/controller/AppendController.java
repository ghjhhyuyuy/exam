package org.wingstudio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.wingstudio.service.PaperService;
import org.wingstudio.service.UserService;

/**
 * Created by wzw on 2018/10/17
 */
@Controller
public class AppendController {
    @Autowired
    UserService userService;
    PaperService paperService;
    /**
     * 返回用户名
     * @return
     */
    @RequestMapping("/get_name")
    public String getName(int uid){
        String name = userService.getName(uid);
        return name;
    }
    /**
     * 返回身份
     */
    @RequestMapping("/get_limit")
    public String getLimit(int uid){
        String name = userService.getIdentity(uid);
        return name;
    }
    /**
     * 返回试卷名称
     */
    @RequestMapping("/get_papername")
    public String getPaperName(int id){
        String name = paperService.getPaperName(id);
        return name;
    }
}
