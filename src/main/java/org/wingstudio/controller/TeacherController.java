package org.wingstudio.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.wingstudio.service.PaperService;

import java.sql.SQLException;

/**
 * Created by wzw on 2018/10/8
 */
@Controller
public class TeacherController {
    @Autowired
    PaperService paperService;
    /**
     * 添加问题
     */
    @RequestMapping("/add_problem")
    public String addProblem(String topic,int id,int type,String answer,int fen){
        String string = paperService.addProblem(topic, id, type, answer, fen);
        return string;
    }
    /**
     * 创建试卷
     */
    @RequestMapping("/add_paper")
    public String addPaper(String name){
        String result = paperService.addPaper(name);
        return result;
    }
    /**
     * 添加题库
     */
    @RequestMapping("/add_bank")
    public ModelAndView addBank(){
        //???
        return null;
    }
    /**
     * 显示所有题库
     */
    @RequestMapping("/show_bank")
    public ModelAndView showBank() throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray = paperService.AllBank();
        modelAndView.addObject("bank",jsonArray);
        return modelAndView;
    }
    /**
     * 显示选择的题库的全部题
     */
    @RequestMapping("/show_bank_problem")
    public ModelAndView showBankProblem(int bid) throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray = paperService.AllBankProblem(bid);
        modelAndView.addObject("bankProblem",jsonArray);
        return modelAndView;
    }
    /**
     * 根据关键字查找题
     */
    @RequestMapping("/selectByWords")
    public ModelAndView selectByWords(String words){
        ModelAndView modelAndView = new ModelAndView();
        JSONObject jsonObject = paperService.selectByWords(words);
        modelAndView.addObject("resultProblem",jsonObject);
        return  modelAndView;
    }
    /**
     * 从题库选择题组成试卷
     */

    /**
     * 选定试卷答题情况统计反馈
     */
    @RequestMapping("/showScore")
    public ModelAndView showScore(int id) throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray = paperService.selectScore(id);
        modelAndView.addObject("showScore",jsonArray);
        return  modelAndView;
    }
    /**
     * 选定试卷展示答题人
     */
    @RequestMapping("/showStudents")
    public ModelAndView showStudents(int id) throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray = paperService.selectStudents(id);
        modelAndView.addObject("showStudents",jsonArray);
        return  modelAndView;
    }
    /**
     * 将指定的答题集按班级分
     */
    @RequestMapping("/divideClass")
    public ModelAndView divideClass(JSONArray jsonArray){
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray1 = paperService.divideClass(jsonArray);
        modelAndView.addObject("resultClass",jsonArray1);
        return modelAndView;
    }
    /**
     * 选定答题人展示试卷列表
     */
    @RequestMapping("/chooseMan")
    public ModelAndView chooseMan(int uid) throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray = paperService.manPaper(uid);
        modelAndView.addObject("manPaper",jsonArray);
        return modelAndView;
    }
    /**
     * 选定试卷和答题人展示具体回答内容
     */
    @RequestMapping("/chooseDetails")
    public ModelAndView chooseDetails(int uid,int id) throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        JSONArray jsonArray = paperService.paperDetails(uid,id);
        modelAndView.addObject("paperDetails",jsonArray);
        return modelAndView;
    }
}
