package org.wingstudio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.wingstudio.service.UserService;

/**
 * Created by wzw on 2018/10/8
 */
@Controller
public class LoginController {
    @Autowired
    UserService userService;
    /**
     * 显示登陆页面
     */
    @RequestMapping("/to_login")
    public String toLogin(){
        return "examIndex";
    }
    @RequestMapping("/to_top")
    public String toTop(){
        return "examTop";
    }
    /**
     * 登陆验证，判断身份？？？
     */
    @RequestMapping("/do_login")
    public String doLogin(String account,String password){
        String result = userService.Login(account,password);
        return result;
    }
}
