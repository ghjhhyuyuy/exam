<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>用户增加</h1>
		<hr>
		<form action="<%=basePath%>userController/add.do" method="post">
			<table>
				<tr>
					<td>姓名：</td>
					<td><input type="text" name="name"></td>
				</tr>
				<tr>
					<td>年龄</td>
					<td><input type="text" name="age"></td>
				</tr>
				<tr>
					<td>性别</td>
					<td><input type="text" name="sex"></td>
				</tr>
				<tr>
					<td>爱好</td>
					<td><input type="text" name="hobby"></td>
				</tr>
				<tr>
					<td>登录名</td>
					<td><input type="text" name="loginName"></td>
				</tr>
				<tr>
					<td><input type="submit" value="增加"></td>
					<td><input type="reset" value="重置"></td>
				</tr>


			</table>

		</form>
	</center>
</body>
</html>